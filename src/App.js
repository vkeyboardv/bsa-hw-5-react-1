import React from 'react';
import Chat from './components/Chat';
import logo from './logo.svg';
// import './App.css';
import './Chat.css';

class App extends React.Component {
  render() {
    return (<div className="App">
      <div className="App-wrapper">
        <img src={logo} className="App-logo" alt="logo" />
        {/* <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> */}
        <Chat />
      </div>
    </div>)
  }
}

export default App;
