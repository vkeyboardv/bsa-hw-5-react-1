import React from 'react';

class MessagesCounter extends React.Component {
  render() {
    return (<div className="main-chat-header-messages-counter">
      Messages: <span>{this.props.messages.length}</span>
    </div>);
  }
}

export default MessagesCounter;
