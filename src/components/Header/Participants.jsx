import React from 'react';
import lodash from 'lodash';

class Participants extends React.Component {
  render() {
    return (<div className="main-chat-participants-counter">
      Participants: <span>{lodash.uniqBy(this.props.messages, 'user').length}</span>
    </div>);
  }
}

export default Participants;
