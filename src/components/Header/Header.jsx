import React from 'react';
import Participants from './Participants';
import MessagesCounter from './MessagesCounter';
import LastMessageDate from './LastMessageDate';

class Header extends React.Component {
  render() {
    return (
      <div className="main-chat-header">
        <div className="main-chat-name">
          <h2>Chat Room</h2>
        </div>
        <div className="main-chat-participants">
          <Participants messages={this.props.messages} />
        </div>
        <div className="main-chat-header-messages">
          <MessagesCounter messages={this.props.messages} />
        </div>
        <div className="main-chat-last-message-date">
          <LastMessageDate messages={this.props.messages} />
        </div>
      </div>
    );
  }
}

export default Header;
