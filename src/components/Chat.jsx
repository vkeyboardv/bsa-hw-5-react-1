import React from 'react';
import Header from './Header/Header';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import SpinnerElement from './SpinnerElement';

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: []
    }
    this.updateState = this.updateState.bind(this);
  }

  componentDidMount() {
    fetch("https://api.myjson.com/bins/1hiqin")
      .then(response => response.json())
      .then(messages => this.setState({ messages }))
      .catch(error => new Error(error));
  }

  updateState(state) {
    this.setState({ state });
  }

  render() {
    if (this.state.messages.length) {
      return (
        <div className="main-chat">
          <Header messages={this.state.messages} />
          <MessageList messages={this.state.messages} currentState={this.updateState} />
          <MessageInput messages={this.state.messages} currentState={this.updateState} />
        </div>
      );
    } else {
      return (
        <div className="spinner">
          <SpinnerElement />
        </div>
      );
    }
  }
}

export default Chat;
