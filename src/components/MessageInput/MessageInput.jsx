import React from 'react';
import uuidv4 from 'uuid/v4';
import moment from 'moment';
import {
  InputGroup,
  InputGroupAddon,
  Input,
  Button,
} from 'reactstrap';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      msg: ''
    };

    // this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
  }

  // handleChange(event) {
  //   this.setState({value: event.target.value});
  // }

  // handleSubmit(event) {
  //   alert('Sent message: ' + this.state.value);
  //   event.preventDefault();
  // }

  handleAddMsg() {
    const { messages, currentState } = this.props;
    const { msg } = this.state;
    if (msg.trim().length > 0) {
      const newMsg = {
        id: uuidv4(),
        user: 'Vladislav',
        avatar: "https://bizraise.pro/wp-content/uploads/2014/09/no-avatar-300x300.png",
        created_at: `${moment().format('YYYY-MM-DD HH:mm:ss')}`,
        message: this.state.msg,
        marked_read: false
      }
      messages.push(newMsg);
      currentState(messages);
      this.setState({ msg: '' });
    }
  }

  render() {
    return (
      <div className='message-input-block'>
        <InputGroup size="sm">
          <Input type="text" onChange={ev => this.setState({ msg: ev.target.value })} value={this.state.msg} />
          <InputGroupAddon addonType="prepend"><Button type="submit" onClick={ev => this.handleAddMsg()}>Send</Button></InputGroupAddon>
        </InputGroup>
      </div>
    );
  }
}

export default MessageInput;
