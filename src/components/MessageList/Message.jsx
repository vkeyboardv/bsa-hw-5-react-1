import React from 'react';
import { Badge } from 'reactstrap'

const myself = 'Vladislav';

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLiked: false
    }
  }

  hanldeDeleteMsg(ev) {
    const { message, messages, currentState } = this.props;

    for (const oldMessage of messages) {
      if (oldMessage.id === message.id) {
        messages.splice(messages.indexOf(oldMessage), 1);
      }
    }

    currentState(messages);
  }

  render() {
    const { isLiked } = this.state;

    return (<div className={this.props.message.user === myself ? 'message myMessage' : 'message'}>
      <div className="message-avatar"><img src={this.props.message.avatar} alt="" /></div>
      <div className="message-info">
        <div className="message-info-user">{this.props.message.user}</div>
        <div className="message-info-message">{this.props.message.message}</div>
        <div className="message-info-created_at">{this.props.message.created_at}</div>
        <div className="message-info-actions">
          <div className="message-info-is-liked"><Badge color="danger" onClick={ev => this.setState({ isLiked: !isLiked })}>{isLiked && this.props.message.user !== myself ? 1 : 0} {' '} likes</Badge></div>
          {this.props.message.user === myself &&
            <div className="message-info-edit-comment">
              <Badge color="info">Edit</Badge>
            </div>}
          {this.props.message.user === myself &&
            <div className="message-info-delete-comment">
              <Badge color="dark" onClick={ev => this.hanldeDeleteMsg(ev)}>X</Badge>
            </div>}
        </div>
      </div>
    </div>)
  }
}

export default Message;
