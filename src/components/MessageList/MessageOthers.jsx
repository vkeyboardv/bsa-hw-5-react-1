import React from 'react';
import Message from './Message';

class MessageOthers extends React.Component {

  getMessages() {
    const messagesArray = this.props.messages.map(message => <Message message={message} key={message.id} messages={this.props.messages} currentState={this.props.currentState} />);
    
    return messagesArray;
  }

  render() {
    return (<div className="messages">
      {this.getMessages()}
    </div>);
  }
}

export default MessageOthers;
