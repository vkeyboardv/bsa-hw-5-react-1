import React from 'react';
import MessageOthers from './MessageOthers';
// import MessageMine from './MessageMine';

class MessageList extends React.Component {
  render() {
    return (
      <div className="main-chat-messages">
        <div className="main-chat-messages-others">
          <MessageOthers messages={this.props.messages} currentState={this.props.currentState} />
        </div>
        {/* <div className="main-chat-messages-mine">
          <MessageMine messages={this.props.messages} />
        </div> */}
      </div>
    );
  }
}

export default MessageList;
